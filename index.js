const Dat = require("dat-node");
const Electron = require("electron");

var archive;

Dat("public", (error, dat) => {
  if(error) {
    console.error(error);
    throw error;
  } else {
    dat.importFiles({watch: true});
    dat.joinNetwork();
    archive = dat;
  }
});

var window;

function createWindow() {
  window = new Electron.BrowserWindow({
    height: 720,
    show: false,
    width: 1280
  });
  window.setMenu(null);
  window.loadFile("index.htm");
  window.once("ready-to-show", () => {
    window.show();
  });
  window.on("closed", () => {
    win = null;
  });
}

Electron.app.on("ready", createWindow);

Electron.app.on("window-all-closed", () => {
  if(process.platform !== "darwin") {
    archive.leaveNetwork();
    Electron.app.quit();
  }
});

Electron.app.on("activate", () => {
  if(window === null) {
    createWindow();
  }
});
